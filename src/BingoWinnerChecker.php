<?php

use Models\Card;
use src\BingoCaller;


class BingoWinnerChecker
{
  public static function isWinner(BingoCaller $caller, Card $card){
    $cardNumbers = $card->getNumbersInCard();

    foreach ($cardNumbers as $cardNumber){
      if(is_null($cardNumber)) //Espacio Libre
        continue;

      if(!$caller->hasCalledNumber($cardNumber)){
        return false;
      }
    }

    return true;
  }
}