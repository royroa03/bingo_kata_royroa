<?php

use PHPUnit\Framework\TestCase;

class BingoWinnerCheckerTest extends TestCase
{
  /** @test */
  public function testCheckerDeterminesNotWinnerYet(){

    $caller = new BingoCaller();

    $card = (new BingoCardGenerator())->generate();

    //Solo 20 números
    for($i=1;$i<=20;$i++){
      $caller->callNumber();
    }

    $this->assertFalse(BingoWinnerChecker::isWinner($caller,$card));


  }

  /** @test */
  public function testCheckerDeterminesAWinnerCorrectly(){

    $caller = new BingoCaller();
    $card = (new BingoCardGenerator())->generate();

    //Carta Ganadora
    for($i=1;$i<=75;$i++){
      $caller->callNumber();
    }

    $this->assertTrue(BingoWinnerChecker::isWinner($caller,$card));

  }
}