<?php

use PHPUnit\Framework\TestCase;

class BingoCardGeneratorTest extends TestCase
{
  /** @test */
  public function testCardContainsValidNumbersRespectingColumnBoundaries(){

    $generator = new BingoCardGenerator();

    $card = $generator->generate();

    $this->assertTrue($card->isValid());

  }

  /** @test */
  public function testCardHasFreeSpaceInTheMiddle(){

    $generator = new BingoCardGenerator();
    $card = $generator->generate();
    $this->assertTrue($card->hasFreeSpaceInTheMiddle());

  }
}